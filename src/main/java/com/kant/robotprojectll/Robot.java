/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.robotprojectll;

/**
 *
 * @author MSI
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(int x, int y, char symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch(direction){
            case 'w':
                if(map.inMap(x, y-1)){
                    y = y - 1;
                }else {
                    return false;
                }                              
                break;
                
            case 's':
                if(map.inMap(x, y+1)){
                    y = y + 1;
                }else {
                    return false;
                }                 
                break;
                
            case 'd':
                if(map.inMap(x + 1, y)){
                    x = x + 1;
                }else {
                    return false;
                } 
                break;
                
            case 'a':
                if(map.inMap(x-1, y)){
                    x = x - 1;
                }else {
                    return false;
                }                
                break;
            default:
                return false;
            
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x, y)){
            System.out.println("Founded Bomb!!! (" + x + ", " + y + ")");
        }
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

}
